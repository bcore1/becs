function bcomponent_view(registry, components) {
    local components_len = components.len()
    local dense = array(components_len)
    local sparse = array(components_len)

    foreach (i, component_id in components) {
        local sparse_set = registry.getComponents(component_id)
        if (sparse_set == null) {
            return null
        }

        dense[i] = sparse_set.dense
        sparse[i] = sparse_set.sparse
    }

    // Find smallest array
    local smallest_dense = null
    foreach (components in dense) {
        if (smallest_dense == null) {
            smallest_dense = components
        } else if (components.len() < smallest_dense.len()) {
            smallest_dense = components
        }
    }

    local result = {entity_id = -1, components = array(components_len)}
    foreach (index in smallest_dense) {
        local valid = true
        foreach (i, components in sparse) {
            if ((result.components[i] = components[index]) == null) {
                valid = false
                break
            }
        }
        
        if (valid) {
            result.entity_id = index
            yield result
        }
    }

    return null
}