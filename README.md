# BECS - Library to write code with composition using the Entity Component System for Gothic 2 Online

```js
class Component1 {
    value = 0
}

class Component2 {
    value = 1
}

local registry = BEntityRegistry()
for (local i = 0; i < 100; ++i) {
    local e = registry.create()
    e.assign(Component1())

    if (i % 4 == 0) {
        e.assign(Component2())
    }
}

local entity = registry.get(5)
local component = entity.get(Component1)
component.value = 5

foreach (entry in bcomponent_view(registry, [Component1, Component2])) {
    local comp1 = entry.components[0]
    local comp2 = entry.components[1]
    print(entry.entity_id + ": " + comp1.value + ", " + comp2.value)
}
```