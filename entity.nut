class BEntity {
    _id = -1
    _registry = null

    constructor(id, registry) {
        this._id = id
        this._registry = registry
    }

    function id() {
        return this._id
    }

    function assign(component) {
        this._registry.assignEntityComponent(this._id, component)
    }

    function remove(component_id) {
        this._registry.removeEntityComponent(this._id, component_id)
    }

    function get(component_id) {
        return this._registry.getEntityComponent(this._id, component_id)
    }
}