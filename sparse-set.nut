class BSparseSet {
    dense = null
    sparse = null

    constructor(size) {
        this.dense = []
        this.sparse = array(size, null)
    }

    function get(index) {
        return this.sparse[index]
    }

    function set(index, component) {
        if (this.sparse[index] == null) {
            this.dense.push(index)
        }

        this.sparse[index] = component
    }

    function remove(index) {
        this.sparse[index] = null

        local len = this.dense.len()
        foreach (i, value in this.dense) {
            if (value == index) {
                local value = this.dense.pop()
                if (i < len - 1) {
                    this.dense[i] = value
                }
                return
            }
        }
    }
}