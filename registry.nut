class BEntityRegistry {
    _entities = null
    _free_idx = null
    _components = null

    constructor() {
        this._entities = []
        this._free_idx = []
        this._components = {}
    }

    function get(id) {
        return this._entities[id]
    }

    function create() {
        local index = this._entities.len()
        if (this._free_idx.len() == 0) {
            this._entities.push(null)

            // resize components memory pool
            foreach (components in this._components) {
                components.sparse.push(null)
            }
        } else {
            index = this._free_idx.pop()
        }

        local entity = BEntity(index, this)
        this._entities[index] = entity

        return entity
    }

    function destroy(entity_id) {
        local entity = this._entities[entity_id]
        entity._id = -1 // Invalidate

        this._entities[entity_id] = null
        this._free_idx.push(entity_id)

        foreach (components in this._components) {
            components.remove(entity_id)
        }
    }

    function getComponents(component_id) {
        if (component_id in this._components) {
            return this._components[component_id]
        }

        return null
    }

    function getEntityComponent(entity_id, component_id) {
        if (component_id in this._components) {
            return this._components[component_id].get(entity_id)
        }

        return null
    }

    function assignEntityComponent(entity_id, component) {
        local component_id = component.getclass()
        local entities_len = this._entities.len()

        if (!(component_id in this._components)) {
            this._components[component_id] <- BSparseSet(entities_len)
        }

        this._components[component_id].set(entity_id, component)
    }

    function removeEntityComponent(entity_id, component_id) {
        if (component_id in this._components) {
            this._components[component_id].remove(entity_id)
        }
    }
}